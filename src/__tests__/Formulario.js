import React from "react";
import { render, screen, cleanup, fireEvent, waitFor } from '@testing-library/react'
import Formulario from "../components/Formulario";
import '@testing-library/jest-dom/extend-expect';
import userEvent from "@testing-library/user-event";
import App from "../App";

// Limpia componente antes de cada test
afterEach( cleanup );

let component;
let crearCita = jest.fn()

beforeEach(() => {
    crearCita = jest.fn();
    component = render(<Formulario crearCita={crearCita}/>);
    const mascota = screen.getByLabelText('Nombre Mascota');
    const propietario = screen.getByLabelText('Nombre Dueño');
    const fecha = screen.getByLabelText('Fecha');
    const hora = screen.getByLabelText('Hora');
    const sintomas = screen.getByLabelText('Sintomas');
})

test('<Formulario /> Cargar el formulario y revisar que todo sea correcto', () => {
    // const wrapper = render(<Formulario />);
    // wrapper.debug();

    // Forma antigua
    // const { getByText } = render(<Formulario />);
    // expect( getByText('Crear Cita') ).toBeInTheDocument();

    // Heading
    const title = component.getByText('Crear Cita');
    expect( title.tagName ).toBe('H2');

    component.getByLabelText('Nombre Mascota');
    component.getByLabelText('Nombre Dueño');
    const date = component.getByLabelText('Fecha');
    const time = component.getByLabelText('Hora');
    component.getByLabelText('Sintomas');

    expect(date).toHaveAttribute('type','date')
    expect(time).toHaveAttribute('type','time')

    // Boton submit
    const addDate = component.getByText('Agregar Cita');
    expect( addDate.tagName ).toBe('BUTTON');
});

// Validación de Formulario
test('<Formulario /> Validación de formulario', async () => {
    // Click en el boton submit
    const btnSubmit = component.getByText('Agregar Cita');
    fireEvent.click(btnSubmit);

    await waitFor(() => {
        const errors = component.getAllByText('El campo es requerido');
        expect(errors.length).toBe(5);
        expect( crearCita ).toHaveBeenCalledTimes(0);
    });
});

// Validación de Formulario (Escribiendo Mascota)
test('<Formulario /> Escribiendo en formulario (Mascota)', async () => {
    // Forma vieja para escribir en el input
    fireEvent.change(mascota, {
        target: {value: ''}
    });

    // Forma nueva para escribir en el input
    userEvent.type(propietario, 'Jorge');
    userEvent.type(fecha, '2021-09-10');
    userEvent.type(hora, '10:30');
    userEvent.type(sintomas, 'Solo duerme');

    // Click en el boton submit
    const btnSubmit = screen.getByText('Agregar Cita');
    userEvent.click(btnSubmit);

    await waitFor(() => {
        const errors = screen.queryAllByText('El campo es requerido');
        expect(errors.length).toBe(1)
        expect( crearCita ).not.toHaveBeenCalled();
    });
});

// Validación de Formulario (Escribiendo Propietario)
test('<Formulario /> Escribiendo en formulario (Propietario)', async () => {

    // Forma nueva para escribir en el input
    userEvent.type(mascota, 'Hook');
    userEvent.type(propietario, '');
    userEvent.type(fecha, '2021-09-10');
    userEvent.type(hora, '10:30');
    userEvent.type(sintomas, 'Solo duerme');

    // Click en el boton submit
    const btnSubmit = screen.getByText('Agregar Cita');
    userEvent.click(btnSubmit);

    await waitFor(() => {
        const errors = screen.queryAllByText('El campo es requerido');
        expect(errors.length).toBe(1);
        expect( crearCita ).not.toHaveBeenCalled();
    });
});

// Validación de Formulario (Escribiendo Fecha)
test('<Formulario /> Escribiendo en formulario (Fecha)', async () => {

    // Forma nueva para escribir en el input
    userEvent.type(mascota, 'Hook');
    userEvent.type(propietario, 'Jorge');
    userEvent.type(fecha, '');
    userEvent.type(hora, '10:30');
    userEvent.type(sintomas, 'Solo duerme');

    // Click en el boton submit
    const btnSubmit = screen.getByText('Agregar Cita');
    userEvent.click(btnSubmit);

    await waitFor(() => {
        const errors = screen.queryAllByText('El campo es requerido');
        expect(errors.length).toBe(1)
        expect( crearCita ).toHaveBeenCalledTimes(0);
    });
});

// Validación de Formulario (Escribiendo Hora)
test('<Formulario /> Escribiendo en formulario (Hora)', async () => {

    // Forma nueva para escribir en el input
    userEvent.type(mascota, 'Hook');
    userEvent.type(propietario, 'Jorge');
    userEvent.type(fecha, '2021-09-10');
    userEvent.type(hora, '');
    userEvent.type(sintomas, 'Solo duerme');

    // Click en el boton submit
    const btnSubmit = screen.getByText('Agregar Cita');
    userEvent.click(btnSubmit);

    await waitFor(() => {
        const errors = screen.queryAllByText('El campo es requerido');
        expect(errors.length).toBe(1)
        expect( crearCita ).toHaveBeenCalledTimes(0);
    });
});

// Validación de Formulario (Escribiendo Sintomas)
test('<Formulario /> Escribiendo en formulario (Sintomas)', async () => {

    // Forma nueva para escribir en el input
    userEvent.type(mascota, 'Hook');
    userEvent.type(propietario, 'Jorge');
    userEvent.type(fecha, '2021-09-10');
    userEvent.type(hora, '10:30');
    userEvent.type(sintomas, '');

    // Click en el boton submit
    const btnSubmit = screen.getByText('Agregar Cita');
    userEvent.click(btnSubmit);

    await waitFor(() => {
        const errors = screen.queryAllByText('El campo es requerido');
        expect(errors.length).toBe(1)
        expect( crearCita ).toHaveBeenCalledTimes(0);
    });
});

// Pasa Validación de Formulario
test('<Formulario /> Pasa Validación de Formulario', async () => {

    // Forma nueva para escribir en el input
    userEvent.type(mascota, 'Hook');
    userEvent.type(propietario, 'Jorge');
    userEvent.type(fecha, '2021-09-10');
    userEvent.type(hora, '10:30');
    userEvent.type(sintomas, 'Solo duerme');

    // Click en el boton submit
    const btnSubmit = screen.getByText('Agregar Cita');
    userEvent.click(btnSubmit);

    await waitFor(() => {
        const errors = screen.queryAllByText('El campo es requerido');
        expect(errors.length).toBe(0)
        expect( crearCita ).toHaveBeenCalledTimes(1);
    });
});
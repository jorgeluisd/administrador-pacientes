import React from "react";
import { render, screen, cleanup, fireEvent, waitFor } from '@testing-library/react'
import Formulario from "../components/Formulario";
import '@testing-library/jest-dom/extend-expect';
import userEvent from "@testing-library/user-event";
import App from "../App";

let component;
beforeEach(() => {
    component = render(<App />);
    const mascota = screen.getByLabelText('Nombre Mascota');
    const propietario = screen.getByLabelText('Nombre Dueño');
    const fecha = screen.getByLabelText('Fecha');
    const hora = screen.getByLabelText('Hora');
    const sintomas = screen.getByLabelText('Sintomas');
})

test('<App /> La aplicación funciona bien la primera vez', () => {
    const title = component.getByText('Administrador de Pacientes');
    expect(title.tagName).toBe('H1');

    component.getByText('Crear Cita');
    component.getByText('No hay citas');
    expect(JSON.parse(localStorage.getItem('citas'))).toEqual([]);
});

test('<App /> Agregar una cita y verificar el Heading', async() => {
    // Forma nueva para escribir en el input
    userEvent.type(mascota, 'Hook');
    userEvent.type(propietario, 'Jorge');
    userEvent.type(fecha, '2021-09-10');
    userEvent.type(hora, '10:30');
    userEvent.type(sintomas, 'Solo duerme');

    // Click en el boton submit
    const btnSubmit = screen.getByText('Agregar Cita');
    userEvent.click(btnSubmit);

    await waitFor(() => {
        component.getByText('Administra tus citas');
        expect(JSON.parse(localStorage.getItem('citas'))).not.toEqual([]);
    });
});

test('<App /> Verificar las citas en el DOM', async() => {

    const citas = await screen.findAllByTestId('cita');

    // Crea un archivo para verificar su contenido
    // expect(citas).toMatchSnapshot();

    expect(screen.getByText('Eliminar ×').tagName).toBe('BUTTON');
    expect(screen.getByText('Eliminar ×')).toBeInTheDocument();

    // Verificar alguna cita
    expect(screen.getByText('Hook')).toBeInTheDocument();
});

test('<App /> Eliminar cita y verificar eliminacion', async() => {

    const btnEliminar = screen.queryByText('Eliminar ×');
    expect(btnEliminar.tagName).toBe('BUTTON');
    expect(btnEliminar).toBeInTheDocument();

    // Simular el click
    userEvent.click(btnEliminar);

    // El boton ni la cita deben estar
    expect(btnEliminar).not.toBeInTheDocument();
    expect(screen.queryByText('Hook')).not.toBeInTheDocument();
    expect(screen.queryByTestId('cita')).not.toBeInTheDocument();
});
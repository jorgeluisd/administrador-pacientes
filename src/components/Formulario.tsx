import React, {Fragment} from "react";
import {v4 as uuid} from 'uuid';
import PropTypes from 'prop-types';
import {SubmitHandler, useForm} from "react-hook-form";
import { ICita, CrearCita } from '../types';

const Formulario = ({crearCita}: {crearCita: CrearCita}) => {

    // Usamos useForm
    const { register, handleSubmit, formState: { errors }, reset, setFocus } = useForm<ICita>();

    // Submit del formulario
    const submitCita: SubmitHandler<ICita> = (data):void => {
        // e.preventDefault();

        // Asignar ID
        data.id = uuid()

        // Crear la cita
        crearCita(data);

        // Reiniciar el form
        reset({});

        // Coloca el cursor en mascotas
        setFocus("mascota");
    }
    // console.log(watch("mascota")); // watch input value by passing the name of it
    return (
        <Fragment>
            <h2>Crear Cita</h2>

            <form
                onSubmit={handleSubmit(submitCita)}
            >
                <label htmlFor="mascota">Nombre Mascota</label>
                <input
                    type="text"
                    id='mascota'
                    {...register("mascota",
                        { required: {value: true, message: "El campo es requerido"}
                        }
                    )}
                    placeholder="Ingrese nombre mascota"
                    className="u-full-width"
                />
                {errors.mascota ? <p>{errors.mascota?.message}</p> : null}

                <label htmlFor="propietario">Nombre Dueño</label>
                <input
                    type="text"
                    id='propietario'
                    {...register("propietario",
                        { required: {value: true, message: "El campo es requerido"}
                        }
                    )}
                    placeholder="Ingrese nombre del propietario"
                    className="u-full-width"
                />
                {errors.propietario ? <p>{errors.propietario?.message}</p> : null}

                <label htmlFor="fecha">Fecha</label>
                <input
                    type="date"
                    id='fecha'
                    {...register("fecha",
                        { required: {value: true, message: "El campo es requerido"}
                        }
                    )}
                    className="u-full-width"
                />
                {errors.fecha ? <p>{errors.fecha?.message}</p> : null}

                <label htmlFor="hora">Hora</label>
                <input
                    type="time"
                    id='hora'
                    {...register("hora",
                        { required: {value: true, message: "El campo es requerido"}
                        }
                    )}
                    className="u-full-width"
                />
                {errors.hora ? <p>{errors.hora?.message}</p> : null}

                <label htmlFor="sintomas">Sintomas</label>
                <textarea
                    className="u-full-width"
                    {...register("sintomas",
                        { required: {value: true, message: "El campo es requerido"}
                        }
                    )}
                    id="sintomas"
                    cols={30}
                    rows={10}
                />
                {errors.sintomas ? <p>{errors.sintomas?.message}</p> : null}

                <button
                    type="submit"
                    className="u-full-width button-primary"
                >
                    Agregar Cita
                </button>
            </form>
        </Fragment>
    );
}

Formulario.propTypes = {
    crearCita: PropTypes.func.isRequired
}
export default Formulario;
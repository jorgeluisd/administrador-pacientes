export interface ICita {
    id:string;
    mascota: string;
    propietario: string;
    fecha: string;
    hora: string;
    sintomas: string;
}

export type EliminarCita = (id:string) => void;

export type CrearCita = (cita:ICita) => void;
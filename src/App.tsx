import React, { Fragment, useState, useEffect } from "react";
import Formulario from "./components/Formulario";
import Cita from "./components/Cita"
import {CrearCita, EliminarCita, ICita} from './types';

// e: React.FormEvent<HTMLFormElement>
function App(): JSX.Element {

    // Citas en local storage
    let citasIniciales:ICita[] = JSON.parse(localStorage.getItem('citas') as string);
    if (!citasIniciales) {
        citasIniciales = [];
    }

    // Arreglo de citas
    const [citas, guardarCitas] = useState<ICita[]>(citasIniciales);

    // Use Effect para realizar ciertas operaciones cunado el state cambia
    useEffect(() => {
        if (citasIniciales) {
            localStorage.setItem('citas', JSON.stringify(citas))
        } else {
            localStorage.setItem('citas', JSON.stringify([]))
        }
    }, [citas,citasIniciales])

    // Funcion que crea una cita en el state
    const crearCita:CrearCita = (cita) => {
        guardarCitas([
            ...citas,
            cita
        ]);
    }

    // Funcion que elimina una cita por ID
    const eliminarCita:EliminarCita = (id) => {
        const nuevasCitas = citas.filter((cita:ICita) => cita.id !== id)
        guardarCitas(nuevasCitas)
    }

    //Mensaje condicional
    const titulo:string = citas.length === 0 ? 'No hay citas' : 'Administra tus citas';

  return (
      <Fragment>
        <h1>Administrador de Pacientes</h1>
        <div className="container">
            <div className="row">
                <div className="one-half column">
                    <Formulario
                        crearCita = {crearCita}
                    />
                </div>
              <div className="one-half column">
                  <h2>{titulo}</h2>
                  {citas.map((cita: ICita) => (
                      <Cita
                          key={cita.id}
                          cita={cita}
                          eliminarCita={eliminarCita}
                      />
                  ))}
              </div>
            </div>
        </div>
      </Fragment>
);
}

export default App;
